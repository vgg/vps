#!/usr/bin/env python3

## Copyright (C) 2023 David Miguel Susano Pinto <pinto@robots.ox.ac.uk>
##
## Copying and distribution of this file, with or without modification,
## are permitted in any medium without royalty provided the copyright
## notice and this notice are preserved.  This file is offered as-is,
## without any warranty.

## DEPRECATED
##
##   This script is now part of the python-via package:
##
##     * https://pypi.org/project/python-via/
##     * https://gitlab.com/vgg/python-via
##
##   Any future improvements will be done there.  Replace any calls
##   that look like this:
##
##       vfs-upload.py SOME-URL FPATH-1 FPATH-2 FPATH-...
##
##   with:
##
##       python3 -m via.vfs upload --url SOME-URL FPATH-1 FPATH-2 FPATH-...

import argparse
import sys
import warnings
from typing import List

from via.vfs import __main__ as vfs_entry_point


def main(argv: List[str]) -> int:
    warnings.warn(
        (
            "The vfs-upload script has now been merged in the python-via"
            " package.  Use `python -m via.vfs upload` instead."
        ),
        DeprecationWarning,
    )
    argv_parser = argparse.ArgumentParser()
    argv_parser.add_argument(
        "vfs_url",
        help="URL of the VFS server",
    )
    argv_parser.add_argument(
        "project_fpaths",
        nargs="+",
        help="Filepath for the projects to be uploaded",
    )
    args = argv_parser.parse_args(argv[1:])
    return vfs_entry_point.main_upload(args.vfs_url, args.project_fpaths)


if __name__ == "__main__":
    sys.exit(main(sys.argv))

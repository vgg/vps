## Copyright (C) 2023 David Miguel Susano Pinto <pinto@robots.ox.ac.uk>
##
## Copying and distribution of this file, with or without modification,
## are permitted in any medium without royalty provided the copyright
## notice and this notice are preserved.  This file is offered as-is,
## without any warranty.

FROM docker.io/library/debian:bullseye

RUN apt-get update \
    && apt-get install -y \
        g++ \
        cmake \
        libboost-filesystem-dev \
        libleveldb-dev \
        libsnappy-dev \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /root/vfs

COPY ./ ./

RUN cmake . \
    && make

# USAGE:
#   vfs_server <address> <port> <threads> <project_dir> <project_rev_dir> <project_rev_db> <log_dir>

ENTRYPOINT ["src/vfs_server"]
